var CACHE_NAME = "CACHE"

const CACHE_URLS = [
    "/", "index.css", "index.js", "RussoOne-Regular.ttf", "fuwu-pl.min.js", "fuwu.min.js",
    "cards/de/battle_cards.txt", "cards/de/categories.txt", "cards/de/everyone_cards.txt",
    "cards/de/never_have.txt", "cards/de/rules.txt", "cards/de/single_cards.txt",
    "cards/de/toasts.txt", "cards/de/who_would.txt"];

function deleteCaches(){
    return caches
        .keys()
        .then(keys => keys.filter(key => key !== CACHE_NAME))
        .then(keys =>
            Promise.all(
                keys.map(key => caches.delete(key))
            )
        )
}

self.addEventListener("install", event => {
    event.waitUntil(
        caches.open(CACHE_NAME).then(cache => cache.addAll(CACHE_URLS))
    );
});

self.addEventListener("activate", event => {
    // delete any unexpected caches
    event.waitUntil(
        deleteCaches()
    );
});

self.addEventListener("fetch", (event) => {
    // Fetch-First Strategy
    event.respondWith(( async () => {

        const cache = await caches.open(CACHE_NAME).catch(() => undefined)

        try {
            const response = await fetch(event.request, { cache: "no-cache" })

            if (!response.ok) throw new Error("Fetch response not ok")

            await cache?.put(event.request, response.clone())
            return response

        } catch (e) { //fetch failed
            const cachedResponse = await cache?.match(event.request)
            return cachedResponse
        }

    })());
});

setTimeout(deleteCaches)
