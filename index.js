class Card{
    constructor(title, cardText, type, isInfinite){
        this.title = title.trim()
        this.cardText = cardText.trim()
        this.type = type
        this.isInfinite = isInfinite
    }
    clone(){
        return new Card(this.title, this.cardText, this.type)
    }
}

class ActiveRule{
    constructor(title, countDown, text, player){
        this.title = title
        this.countDown = countDown
        this.text = text
        this.player = player
    }
}

class NurOption{
    constructor(name, text, isDisabled){
        this.name = name
        this.text = text
        this.isDisabled = isDisabled ?? defaultIsDisabled
    }
}

const CardType = {
    Everyone: "ev",
    Battle: "ba",
    Single: "si",
    Rule: "ru",
    Interm: "im"
}

const FixedCards = {
    KingCup: new Card("King Cup", "{p1}, fülle zwei Schlücke von einem beliebigen Getränk in einen Becher, den King Cup. Ist der Becher bereits voll, trinke ihn!", CardType.Single),
    Categorie: new Card("Kategorie", "Nennt nacheinader im Uhrzeigersinn #. Wem zuerst nichts mehr einfällt oder wer etwas wiederholt trinkt 4 Schlücke! {p1} beginnt.", CardType.Everyone),
    CustomCategorie: new Card("Kategorie", "{p1} sucht sich eine Kategorie aus. Nennt nacheinader im Uhrzeigersinn Dinge passend zu dieser Kategorie. Wem zuerst nichts mehr einfällt oder wer etwas wiederholt trinkt 4 Schlücke! {p2} beginnt.", CardType.Everyone),
    FuckMarryKill: new Card("Fuck, Marry, Kill", "{p1}, entscheide für {p2}, {p3} und {p4} Fuck Marry Kill. Fuck - Ihr beide trinkt jetzt 4 Schlücke. Marry - Ihr beide trinkt ab sofort immer zusammen. Kill - Diese Person muss 7 Schlücke trinken.", CardType.Single),
    TruthOrDare: new Card("Wahrheit oder Pflicht", "{p1}, stelle {p2}, eine Wahrheit oder Pflicht Frage. Weigert sich {p2}, diese zu machen, muss er/sie 7 Schlücke trinken.", CardType.Single),
    NeverHave: new Card("Ich hab' noch nie...", "...#", CardType.Everyone),
    WhoWould: new Card("Wer würde eher...", "...#? Trinke einen Schluck, immer wenn jemand dich auswählt.", CardType.Everyone),
    Wasted: new Card("Absturz", "{p1}, du wirst heute abstürzen, denn ab jetzt wirst du öfter an die Reihe kommen.", CardType.Rule),
    SpinTheBottle: new Card("Flaschen drehen", "{p1}, drehe zweimal eine Flasche. Die Personen, auf die die beiden Flaschen zeigen trinken 3 Schlücke. Oder küssen sich wahlweise", CardType.Single),
    CrazyCap: new Card("Deckel Drama", "{p1}, nimm einen Kronkorken in die Hand. Trinke danach so viele Schlücke, wie du und andere Spieler zusammen Kronkorken in der Hand haben.", CardType.Single),
    CrazyCapRelease: new Card("Deckel Los", "Alle Spieler dürfen so viele Schlücke verteilen, wie sie Kronkorken dank \"Deckel Drama\" in der Hand haben.", CardType.Everyone),
    Interm: new Card("Zwischenstand", "# wurde bisher am seltensten auf Karten genannt. Trinke also gleich mal 5 Schlücke um aufzuholen. $ hingegen wurde am öftesten genannt und darf deswegen 5 Schlücke verteilen, um das etwas auszugleichen. Dies ist jetzt bereits die %. Karte", CardType.Interm),
    ShotRoulette: new Card("Shot Roulette", "{p1}, stelle einen Shot in die Mitte. Sind dort jetzt insgesamt 4 Shots, trinkst du einen und verteilst die Restlichen. Wählt die Shots zufällig, z.B. blind oder mit einem Würfel.", CardType.Single),
}

class Random{
    constructor(seed){
        if (seed == null)
            seed = Date.now()
        this.seed = seed
    }
    next(){
        let x = Math.sin(this.seed++) * 1_000_000
        return x - Math.floor(x)
    }
    chance(chance){
        return this.next() < chance
    }
}

const defaultColor = "#520c6e"

var data = $fuwuProxy()

const initOptions = ()=>{
    data.appearanceColor = defaultColor
    data.players = []

    data.withNormalCards = true
    data.withKingCup = true
    data.withNeverHave = true
    data.withWhoWould = true
    data.withTruthOrDare = true
    data.withCategories = true
    data.withSpinTheBottle = true
    data.withFuckMarryKill = true
    data.withCrazyCap = true
    data.withShotRoulette = true
}
initOptions()

const initState = ()=>{

    data.isMainMenu = true
    data.isSelectingPlayers = false

    data.cardText = ""
    data.cardTitle = ""
    data.cardIntro = ""
    data.enteringPlayer = false
    data.isIntroducingCard = false
    data.isEndingRule = false
    data.endedRule = {}
    data.randomSeed = Date.now()
    data.currentCard = 0
    data.activeRules = []
    data.wastedPlayer = ""
    data.playerNamedMap = {}

    data.defaultIndex = 0
    data.neverHaveIndex = 0
    data.whoWouldIndex = 0
    data.categorieIndex = 0

    data.lastKingCup = 0
    data.lastNeverHave = 0
    data.lastWhoWould = 0
    data.lastTruthOrDare = 0
    data.lastCategories = 0
    data.lastSpinTheBottle = 0
    data.lastFuckMarryKill = 0
    data.lastCrazyCap = 0
    data.lastInterm = 0
    data.lastShotRoulette = 0
}
initState()

const storageData = window.localStorage.getItem("data")
if (storageData != null && storageData.length > 0){
    const parsedData = JSON.parse(storageData)
    if (parsedData != null){
        for (const [key, value] of Object.entries(parsedData)) {
            if (key.startsWith("$"))
                continue
            
            data[key] = value
        }
    }
}

data.$listeners.push(() => {
    window.localStorage.setItem("data", JSON.stringify(data))

    const color = getHSLColor()
    const root = $o(":root")

    root?.setAttribute("style", `--c-h: ${color.h}; --c-s: ${color.s}%; --c-l: ${color.l}%`)

    $o("meta[name='theme-color'], meta[name='msapplication-navbutton-color']")
        ?.setAttribute("content", data.appearanceColor);
})

const options = [
    new NurOption("data.withNormalCards", "Normale Karten"),
    new NurOption("data.withKingCup", FixedCards.KingCup.title),
    new NurOption("data.withNeverHave", FixedCards.NeverHave.title),
    new NurOption("data.withWhoWould", FixedCards.WhoWould.title),
    new NurOption("data.withTruthOrDare", FixedCards.TruthOrDare.title),
    new NurOption("data.withCategories", "Kategorien"),
    new NurOption("data.withSpinTheBottle", FixedCards.SpinTheBottle.title),
    new NurOption("data.withFuckMarryKill", FixedCards.FuckMarryKill.title),
    new NurOption("data.withCrazyCap", FixedCards.CrazyCap.title),
    new NurOption("data.withShotRoulette", FixedCards.ShotRoulette.title),
]

function getActiveOptionsCount(){
    let all = 0
    for (let i = 0; i < options.length; i++){
        if (eval(options[i].name)){
            all++
        }
    }
    return all;
}

function defaultIsDisabled(){
    const count = getActiveOptionsCount()
    const active = eval(this.name)

    if (count == 1){
        return active
    } else if (count == 2){
        return active && this.name != "data.withFuckMarryKill" && data.withFuckMarryKill
    } else {
        return false
    }
}

function getActiveOptionsRate(){
    return getActiveOptionsCount() / parseFloat(options.length)
}

let random = new Random(data.randomSeed)

const toasts = []

let cardsStack = []
const defaultCards = [FixedCards.Wasted]

let neverHaveStack = []
const neverHaveCards = []

let whoWouldStack = []
const whoWouldCards = []

let categorieStack = []
const categorieCards = []

let cardTypesParsed = 0

const rules = []

let cardsLoaded = false

function nextCard(){
    if (!cardsLoaded){
        return
    }

    if (data.isEndingRule){
        data.isEndingRule = false
        return
    }

    data.isIntroducingCard = !data.isIntroducingCard

    if (data.isIntroducingCard){
        const card = pickNextCard()
        data.cardText = card.cardText
        data.cardTitle = card.title

        switch (card.type) {
            case CardType.Everyone : {
                data.cardIntro = "An Alle"
                break;
            }
            case CardType.Battle : {
                data.cardIntro = "Kampf"
                break;
            }
            case CardType.Single : {
                data.cardIntro = "An {p1}"
                break;
            }
            case CardType.Rule : {
                data.cardIntro = "Regel"
                break;
            }
            case CardType.Interm : {
                data.cardIntro = "Zwischenstand"
                break;
            }
        }

        const p1 = insertPlayerNames()
        checkForRules(card, p1)
    }
}

function pickNextCard(){
    const currentCard = ++data.currentCard;

    const customChance = (data.defaultIndex / parseFloat(currentCard)) * 0.75
    const rate = getActiveOptionsRate()

    const defaultCard = () => {
        const index = data.defaultIndex++ % cardsStack.length
        return cardsStack[index]
    }

    if (random.chance(customChance) || !data.withNormalCards){

        const randomTable = []

        function wr(weight, condition, callback){
            if (condition){
                for (let i = 0; i < weight * 4.0; i++){
                    randomTable.push(callback)
                }
            }
        }

        wr(2, data.withNeverHave && currentCard > data.lastNeverHave + 6 * rate, () => {
            const index = data.neverHaveIndex++ % neverHaveStack.length
            const card = FixedCards.NeverHave.clone()
            card.cardText = card.cardText.replace("#", neverHaveStack[index])

            data.lastNeverHave = currentCard

            return card
        })

        wr(2, data.withWhoWould && currentCard > data.lastWhoWould + 6 * rate, () => {
            const index = data.whoWouldIndex++ % whoWouldStack.length
            const card = FixedCards.WhoWould.clone()
            card.cardText = card.cardText.replace("#", whoWouldStack[index])

            data.lastWhoWould = currentCard

            return card
        })

        wr(1, data.withTruthOrDare && currentCard > data.lastTruthOrDare + 6 * rate, () => {
            data.lastTruthOrDare = currentCard
            return FixedCards.TruthOrDare
        })
        
        wr(1, data.withCategories && currentCard > data.lastCategories + 11 * rate, () => {

            data.lastCategories = currentCard

            if (random.chance(0.2)){
                return FixedCards.CustomCategorie
            }

            const index = data.categorieIndex++ % categorieStack.length
            const card = FixedCards.Categorie.clone()
            card.cardText = card.cardText.replace("#", categorieStack[index])

            return card
        })

        wr(1, data.withFuckMarryKill && data.players.length > 3 && currentCard > data.lastFuckMarryKill + 11 * rate, () => {
            data.lastFuckMarryKill = currentCard
            return FixedCards.FuckMarryKill
        })

        wr(1, data.withSpinTheBottle && currentCard > data.lastSpinTheBottle + 7 * rate, () => {
            data.lastSpinTheBottle = currentCard
            return FixedCards.SpinTheBottle
        })

        wr(3, data.withCrazyCap && currentCard > data.lastCrazyCap + 8 * rate, () => {
            if (random.chance(0.15) && data.lastCrazyCap != 0){
                data.lastCrazyCap = currentCard
                return FixedCards.CrazyCapRelease
            } else {
                data.lastCrazyCap = currentCard
                return FixedCards.CrazyCap
            }
        })

        wr(3, data.withKingCup && currentCard > data.lastKingCup + 8 * rate, () => {
            data.lastKingCup = currentCard
            return FixedCards.KingCup
        })

        wr(2, data.withShotRoulette && currentCard > data.lastShotRoulette + 8 * rate, () => {
            data.lastShotRoulette = currentCard
            return FixedCards.ShotRoulette
        })

        wr((options.length - getActiveOptionsCount()) / 2 + 2, data.withNormalCards, defaultCard)

        wr(1, data.withNormalCards & currentCard > data.lastInterm + 20, () => {
            data.lastInterm = currentCard
            
            const interm = FixedCards.Interm.clone()

            const sortedPlayers = Object.entries(data.playerNamedMap)
            shuffle(sortedPlayers, random)
            sortedPlayers.sort((a , b) => a[1] - b[1])

            const lowest = sortedPlayers[0][0]
            const highest = sortedPlayers[sortedPlayers.length - 1][0]
            playerNamed(lowest)
            playerNamed(highest)

            interm.cardText = interm.cardText.replaceAll("#", lowest)
            interm.cardText = interm.cardText.replaceAll("$", highest)
            interm.cardText = interm.cardText.replaceAll("%", currentCard)
            return interm
        })

        if (randomTable.length > 0){
            return randomTable[Math.floor(randomTable.length * random.next())]()
        }
    }

    if (data.withNormalCards){
        return defaultCard()
    } else {
        return pickNextCard()
    }
}

function checkForRules(card, p1){
    const countDown = 10 + random.next() * 6
    if (card == FixedCards.FuckMarryKill){
        data.activeRules.push(new ActiveRule("Marry", countDown, "Marry - Ihr beide trinkt ab sofort immer zusammen", p1))
    } else if (card == FixedCards.Wasted) {
        data.activeRules.push(new ActiveRule(card.title, countDown + 4, data.cardText, p1))
        data.wastedPlayer = p1
    } else if (rules.includes(card)){
        const isForP1 = card.cardText.includes("{p1}")
        data.activeRules.push(new ActiveRule(card.title, countDown + (card.isInfinite ? 5_000 : 0), data.cardText, isForP1 ? p1 : undefined))
    }

    for (let i = 0; i < data.activeRules.length; i++) {
        const rule = data.activeRules[i];
        rule.countDown--
        if (rule.countDown < 0){
            data.activeRules.splice(i, 1)
            data.isEndingRule = true
            data.endedRule = rule

            if (rule == FixedCards.Wasted){
                data.wastedPlayer = ""
            }

            break
        }
    }
}

function insertPlayerNames(){
    const players = [...data.players]
    const teamPlayers = [...data.players]
    shuffle(teamPlayers, random)
    shuffle(players, random)
    
    const mid = teamPlayers.length / 2
    const t1 = teamPlayers.slice(0, mid)
    const t2 = teamPlayers.slice(mid)

    if (data.wastedPlayer.length > 0){
        const wastedIndex = players.indexOf(data.wastedPlayer)
        let targetIndex = Math.max(0, wastedIndex - 1)
        if (random.chance(0.5)){
            targetIndex = 0
        }

        const swap = players[targetIndex]
        players[targetIndex] = players[wastedIndex]
        players[wastedIndex] = swap
    }

    const map = {
        "{t1}": joinTeam(t1),
        "{t2}": joinTeam(t2),
        "{p1}": players[0],
        "{p2}": players[1],
        "{p3}": players[2],
        "{p4}": players[3]
    }

    const playerCheck = data.cardText + data.cardTitle
    
    for (let i = 1; i <= 4; i++){
        if (playerCheck.includes("{p" + i + "}")){
            playerNamed(players[i-1])
        }
    }

    if (playerCheck.includes("{t1}")){
        t1.forEach(playerNamed)
    }
    if (playerCheck.includes("{t2}")){
        t2.forEach(playerNamed)
    }

    data.cardText = replaceMap(data.cardText, map)
    data.cardTitle = replaceMap(data.cardTitle, map)
    data.cardIntro = replaceMap(data.cardIntro, map)

    return players[0]
}

function joinTeam(t){
    let s = ""
    for (let i = 0; i < t.length; i++) {
        const p = t[i];
        if (i == 0){
            s = p
        } else if (i == t.length - 1) {
            s += ` und ${p}`
        } else {
            s += `, ${p}`
        }
    }
    return s
}

function replaceMap(input, map){

    for (const [key, value] of Object.entries(map)) {
        if (value == null)
            continue
        
        input = input.replaceAll(key, value)
    }

    return input
}

function playerNamed(player){
    if (data.playerNamedMap[player] == null){
        data.playerNamedMap[player] = 0
    }
    data.playerNamedMap[player]++
}

function playerNamedFill(){
    data.players.forEach(p => {
        if (data.playerNamedMap[p] == null){
            data.playerNamedMap[p] = 0
        }
    })
}

function parseCards(body, forEach){
    for (let line of body.split("\n")){
        line = line.trim()
        if (line.length > 0){
            forEach(line)
        }
    }

    cardTypesParsed++
    if (cardTypesParsed == 8){
        cardsLoaded = true
        prepareAllCards()
    }
}

function prepareAllCards(){
    cardsStack = [...defaultCards]
    shuffle(cardsStack, random)

    neverHaveStack = [...neverHaveCards]
    shuffle(neverHaveStack, random)

    whoWouldStack = [...whoWouldCards]
    shuffle(whoWouldStack, random)

    categorieStack = [...categorieCards]
    shuffle(categorieStack, random)
}

function shuffle(array, random){
    const length = array.length
    for (let i = length - 1; i > 0; i--){
        let swapIndex = Math.floor(random.next() * (i + 1))
        let swap = array[i]
        array[i] = array[swapIndex]
        array[swapIndex] = swap
    }
}

function pickPlayers(){
    const count = getActiveOptionsCount()
    if (count > 0){
        if (count == 1 && data.withFuckMarryKill){
            window.alert("Es kann nicht nur 'Fuck, Marry, Kill' gespielt werden")
        } else {
            data.isSelectingPlayers = true
        }
    } else {
        window.alert("Wähle mindestens einen Kartentyp!")
    }
}

function addPlayer(){
    data.enteringPlayer = true
}

function submitPlayer(){
    data.enteringPlayer = false
    let name = $o("#player-name-input").value
    if (name != null && name.trim().length > 0){
        name = name.trim()
        let addName = name
        let addId = 1
        while(data.players.includes(addName)){
            addId++
            addName = name + " " + addId
        }

        data.players.push(addName)
        playerNamedFill()
    }
}

function removePlayer(index){
    const [ removedPlayer ] = data.players.splice(index, 1);
    delete data.playerNamedMap[removedPlayer]
}

function startGame(){
    data.isMainMenu = false
    data.isSelectingPlayers = false
    data.isIntroducingCard = false
    data.isEndingRule = false

    data.cardTitle = "Trinkspruch"
    data.cardText = toasts[ Math.floor(Math.random() * toasts.length) ]

    playerNamedFill()
}



function contentShouldCenter(){
    return !data.isMainMenu && data.isIntroducingCard
}

loadToasts()
loadEveryoneCards()
loadNeverHaveCards()
loadWhoWouldCards()
loadCategorieCards()

function loadToasts(){
    $http.get("cards/de/toasts.txt", (body)=>{
        parseCards(body, (card) => {
            toasts.push(card)
        })
    })
}

function loadEveryoneCards(){
    $http.get("cards/de/everyone_cards.txt", (body)=>{
        parseCards(body, (card) => {
            [title, text, multiple] = card.split("^")

            if (multiple != null){
                multiple = parseInt(multiple.trim())
            } else {
                multiple = 1
            }

            for (let i = 0; i < multiple; i++)
                defaultCards.push(new Card(title, text, CardType.Everyone))
        })

        loadBattleCards()
    })
}

function loadBattleCards(){
    $http.get("cards/de/battle_cards.txt", (body)=>{
        parseCards(body, (card) => {
            [title, text, multiple] = card.split("^")

            if (multiple != null){
                multiple = parseInt(multiple.trim())
            } else {
                multiple = 1
            }

            for (let i = 0; i < multiple; i++)
                defaultCards.push(new Card(title, text, CardType.Battle))
        })

        loadSingleCards()
    })
}

function loadSingleCards(){
    $http.get("cards/de/single_cards.txt", (body)=>{
        parseCards(body, (card) => {
            [title, text, multiple] = card.split("^")

            if (multiple != null){
                multiple = parseInt(multiple.trim())
            } else {
                multiple = 1
            }

            for (let i = 0; i < multiple; i++)
                defaultCards.push(new Card(title, text, CardType.Single))
        })

        loadRuleCards()
    })
}

function loadRuleCards(){
    $http.get("cards/de/rules.txt", (body)=>{
        parseCards(body, (card) => {
            [title, text, multiple] = card.split("^")

            if (multiple != null){
                multiple = parseInt(multiple.trim())
            } else {
                multiple = 1
            }

            let isInfinite = false

            if (text.includes("{inf}")){
                text = text.replaceAll("{inf}", "").trim()
                isInfinite = true
            }

            const c = new Card(title, text, CardType.Rule, isInfinite)

            rules.push(c)

            for (let i = 0; i < multiple; i++)
                defaultCards.push(c)
        })
    })
}

function loadNeverHaveCards(){
    $http.get("cards/de/never_have.txt", (body)=>{
        parseCards(body, (card) => {
            neverHaveCards.push(card)
        })
    })
}

function loadWhoWouldCards(){
    $http.get("cards/de/who_would.txt", (body)=>{
        parseCards(body, (card) => {
            whoWouldCards.push(card)
        })
    })
}

function loadCategorieCards(){
    $http.get("cards/de/categories.txt", (body)=>{
        parseCards(body, (card) => {
            categorieCards.push(card)
        })
    })
}


var burger = $fuwuProxy()

burger.open = false
burger.menu = "main"

function endGame(){
    burger.open = false

    initState()

    random = new Random(data.randomSeed)
    prepareAllCards()
}

function burgerClick(){
    if (!burger.open){
        burger.open = true
        burger.menu = "main"
        playerNamedFill()
    }
}

function burgerClose(){
    burger.open = false
}

function mainMenu(){
    burger.menu = "main"
}

function endGameMenu(){
    burger.menu = "end"
}

function playerMenu(){
    burger.menu = "player"
}

function rulesMenu(){
    burger.menu = "rules"
}

function openSettings(){
    burger.menu = "settings"
    burger.open = true
}

function getHSLColor(){
    const hexMatch = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(data.appearanceColor);
    let 
        r = parseInt(hexMatch[1], 16),
        g = parseInt(hexMatch[2], 16),
        b = parseInt(hexMatch[3], 16);

    r /= 255;
    g /= 255;
    b /= 255;

    const l = Math.max(r, g, b);
    const s = l - Math.min(r, g, b);
    const h = s
        ? l === r
        ? (g - b) / s
        : l === g
        ? 2 + (b - r) / s
        : 4 + (r - g) / s
        : 0;
    return {
        h: 60 * h < 0 ? 60 * h + 360 : 60 * h,
        s: 100 * (s ? (l <= 0.5 ? s / (2 * l - s) : s / (2 - (2 * l - s))) : 0),
        l: (100 * (2 * l - s)) / 2,
    }
}

function resetColor(){
    data.appearanceColor = $o('#color-picker').value = defaultColor
}

function onResize(){
    let scale = window.innerHeight / 1127.0
    scale *= 0.89
    $o("#fixed-wrapper").setAttribute("style", "--scale-size: " + scale)
}

window.addEventListener('resize', onResize, true);

$domReady(onResize)