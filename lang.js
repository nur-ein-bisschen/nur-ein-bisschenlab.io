var lang = {}

const availableLanguages = ["de", "en"]

let langToUse = window.localStorage.getItem("lang")
if (langToUse == null || !availableLanguages.includes(langToUse)){
    let browserLang = window.navigator.userLanguage || window.navigator.language;
    [browserLang] = browserLang.split(/[-_]/);
    let index = availableLanguages.indexOf(browserLang.toLowerCase())
    if (index < 0){
        index = 0
    }
    langToUse = availableLanguages[index]
}

window.localStorage.setItem("lang", langToUse)
lang.code = langToUse

const readLanguage = $http.s.get(`lang/${langToUse}.lang`)

readLanguage.split(/[\r\n]+/).forEach((line) => {
    const equalsIndex = line.indexOf("=")

    if (equalsIndex < 0) return;

    const key = line.slice(0, equalsIndex).trim()
    const value = line.slice(equalsIndex + 1).trim()

    lang[key] = value
})